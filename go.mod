module gitea.com/xorm/gitea_log_bridge2

go 1.12

require (
	code.gitea.io/log v0.0.0-20190526010349-0560851a166a
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
	xorm.io/xorm v1.0.0
)
